let mix = require( 'laravel-mix' );

mix.options( {
    processCssUrls: false,
    cssNano: {
        discardComments: {
            removeAll: true,
        },
    },
    manifest: false
} );

mix.js( [ 'assets-src/settings-sender-address.jsx' ], 'assets/js/settings-sender-address.js' );

