[![pipeline status](https://gitlab.com/wpdesk/wp-settings-sender-address/badges/master/pipeline.svg)](https://gitlab.com/wpdesk/wp-settings-sender-address/pipelines) 
[![coverage report](https://gitlab.com/wpdesk/wp-settings-sender-address/badges/master/coverage.svg?job=unit+test+lastest+coverage)](https://gitlab.com/wpdesk/wp-settings-sender-address/commits/master) 
[![Latest Stable Version](https://poser.pugx.org/wpdesk/wp-settings-sender-address/v/stable)](https://packagist.org/packages/wpdesk/wp-settings-sender-address) 
[![Total Downloads](https://poser.pugx.org/wpdesk/wp-settings-sender-address/downloads)](https://packagist.org/packages/wpdesk/wp-settings-sender-address) 
[![Latest Unstable Version](https://poser.pugx.org/wpdesk/wp-settings-sender-address/v/unstable)](https://packagist.org/packages/wpdesk/wp-settings-sender-address) 
[![License](https://poser.pugx.org/wpdesk/wp-settings-sender-address/license)](https://packagist.org/packages/wpdesk/wp-settings-sender-address) 

# wp-settings-sender-address

Before commit execute:
`npm install` 
`npm run build`
