<?php
/**
 * Settings field.
 */

namespace WpDesk\WooCommerce\ShippingMethod\Fields\SenderAddress;


/**
 * Display field in settings and handle posted data.
 */
class SettingsField {

	const FIELD_TYPE = 'sender_addresses';

	/**
	 * Field name.
	 *
	 * @var string
	 */
	private $field_name;

	/**
	 * SettingsField constructor.
	 *
	 * @param string $field_name .
	 */
	public function __construct( $field_name ) {
		$this->field_name = $field_name;
	}

    /**
     * Get field value from post as JSON.
     *
     * @param array|string $posted_value .
     *
     * @return string
     */
	public function get_field_posted_value_as_json( $posted_value ) {
	    if ( ! empty( $posted_value ) && is_array( $posted_value ) ) {
	        $value = json_encode( $posted_value );
        } else {
            $value = json_encode( array() );
        }
	    return $value;
    }

    /**
     * Render field settings.
     *
     * @param string $field_title
     * @param string $tooltip_html
     * @param string $settings_field_value_as_json .
     * @param Labels $labels .
     */
	public function render(
	    $field_title,
        $tooltip_html,
	    $settings_field_value_as_json,
        $labels
    ) {
		$field_key = $this->field_name;

		$settings_field_value_as_array = json_decode( ! empty( $settings_field_value_as_json ) ? $settings_field_value_as_json: '{}', true );

		if ( empty( $settings_field_value_as_array ) ) {
            $settings_field_value_as_array = array();
        }

        if ( empty( $labels ) ) {
            $labels = new Labels();
        }

		$json_value = json_encode( array_values( $settings_field_value_as_array ) );
		include __DIR__ . '/views/settings-field.php';

	}

}
