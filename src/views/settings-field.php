<?php
/**
 * Display field.
 *
 * @package WpDesk\WooCommerce\ShippingMethod\Fields\SenderAddress
 *
 * @var string $field_key
 * @var string $field_title
 * @var string $tooltip_html
 * @var array  $labels
 * @var string $json_value
 * @var array  $built_in_boxes
 */
?><tr valign="top">
    <th scope="row" class="titledesc">
        <label for="<?php echo esc_attr( $field_key ); ?>"><?php echo wp_kses_post( $field_title ); ?><?php echo $tooltip_html; ?></label>
    </th>
    <td class="forminp">
        <fieldset
            class="settings-field-sender-address"
            id="<?php echo esc_attr( $field_key ); ?>_fieldset"
            data-value="<?php echo esc_attr( $json_value ); ?>"
            data-name="<?php echo esc_attr( $field_key ); ?>"
            data-labels="<?php echo esc_attr( json_encode( $labels, JSON_FORCE_OBJECT ) ); ?>"
        >
        </fieldset>
    </td>
</tr>
