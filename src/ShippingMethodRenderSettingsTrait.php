<?php
/**
 * Render settings HTML.
 *
 * @package WpDesk\WooCommerce\ShippingMethod\Fields\SenderAddress
 */

namespace WpDesk\WooCommerce\ShippingMethod\Fields\SenderAddress;

/**
 * Can render settings html.
 * For use in shipping method when sender_addresses field is used in shipping method settings.
 */
trait ShippingMethodRenderSettingsTrait {

	/**
	 * Prefix key for settings.
	 *
	 * @param  string $key Field key.
	 * @return string
	 */
	public function get_field_key( $key ) {
		/** @var \WC_Shipping_Method $shipping_method */
		$shipping_method = $this;
		return $shipping_method->plugin_id . $shipping_method->id . '_' . $key;
	}

	/**
	 * Get field params
	 *
	 * @param string $key Field key.
	 * @param array $data Data.
	 *
	 * @return array
	 */
	private function get_field_params( $key, $data ) {
		$field_key = $this->get_field_key( $key );
		$defaults  = [
			'field_key'         => $field_key,
			'title'             => '',
			'disabled'          => false,
			'class'             => '',
			'css'               => '',
			'placeholder'       => '',
			'type'              => 'text',
			'desc_tip'          => false,
			'description'       => '',
			'custom_attributes' => [],
			'value'             => '',
		];
		$data      = wp_parse_args( $data, $defaults );

		return $data;
	}

	/**
	 * Generates settings HTML.
	 *
	 * @param string $key .
	 * @param array $data .
	 *
	 * @return string
	 */
	public function generate_sender_addresses_html( $key, $data ) {
		/** @var \WC_Shipping_Method $shipping_method */
		$shipping_method = $this;
		$data['value'] = $shipping_method->get_option( $key );
		$data = $this->get_field_params( $key, $data );
		$settings_field = new SettingsField( $data['field_key'] );
		ob_start();
		$settings_field->render( $data['title'], '', $data['value'], null );
		return ob_get_clean();
	}

	/**
	 * Validate field.
	 *
	 * Convert field to json.
	 *
	 * @param  string $key Field key.
	 * @param  string $value Posted Value.
	 * @return string
	 */
	public function validate_sender_addresses_field( $key, $value ) {
		$settings_field = new SettingsField( $key );
		return $settings_field->get_field_posted_value_as_json( wp_unslash( $value ) );
	}

}