<?php
/**
 * Abstract sender address.
 *
 * @package WpDesk\WooCommerce\ShippingMethod\Fields\SenderAddress
 */

namespace WpDesk\WooCommerce\ShippingMethod\Fields\SenderAddress;

/**
 * Abstract sender address.
 */
abstract class AbstractSenderAddress implements \JsonSerializable {

    const ADDRESS_ID = 'address_id';
	const COMPANY = 'company';
	const NAME = 'name';
	const ADDRESS = 'address';
	const POSTAL_CODE = 'postal_code';
	const CITY = 'city';
	const PHONE = 'phone';
	const EMAIL = 'email';

    /**
     * @var string
     */
    private $address_id = '';

	/**
	 * @var string
	 */
	private $company = '';

    /**
     * @var string
     */
    private $name = '';

	/**
	 * @var string
	 */
	private $address = '';

	/**
	 * @var string
	 */
	private $postal_code = '';

	/**
	 * @var string
	 */
	private $city = '';

	/**
	 * @var string
	 */
	private $phone = '';

	/**
	 * @var string
	 */
	private $email = '';

	/**
	 * @return string
	 */
	public function get_address_id() {
		return $this->address_id;
	}

	/**
	 * @param string $address_id
	 */
	public function set_address_id( $address_id ) {
		$this->address_id = $address_id;
	}

	/**
	 * @return string
	 */
	public function get_company() {
		return $this->company;
	}

	/**
	 * @param string $company
	 */
	public function set_company( $company ) {
		$this->company = $company;
	}

	/**
	 * @return string
	 */
	public function get_name() {
		return $this->name;
	}

	/**
	 * @param string $name
	 */
	public function set_name( $name ) {
		$this->name = $name;
	}

	/**
	 * @return string
	 */
	public function get_address() {
		return $this->address;
	}

	/**
	 * @param string $address
	 */
	public function set_address( $address ) {
		$this->address = $address;
	}

	/**
	 * @return string
	 */
	public function get_postal_code() {
		return $this->postal_code;
	}

	/**
	 * @param string $postal_code
	 */
	public function set_postal_code( $postal_code ) {
		$this->postal_code = $postal_code;
	}

	/**
	 * @return string
	 */
	public function get_city() {
		return $this->city;
	}

	/**
	 * @param string $city
	 */
	public function set_city( $city ) {
		$this->city = $city;
	}

	/**
	 * @return string
	 */
	public function get_phone() {
		return $this->phone;
	}

	/**
	 * @param string $phone
	 */
	public function set_phone( $phone ) {
		$this->phone = $phone;
	}

	/**
	 * @return string
	 */
	public function get_email() {
		return $this->email;
	}

	/**
	 * @param string $email
	 */
	public function set_email( $email ) {
		$this->email = $email;
	}

    /**
     * JSON serialize.
     *
     * @return array
     */
    public function jsonSerialize() {
        return
            [
                self::ADDRESS_ID  => $this->get_address_id(),
                self::COMPANY     => $this->get_company(),
                self::NAME        => $this->get_name(),
                self::ADDRESS     => $this->get_address(),
                self::POSTAL_CODE => $this->get_postal_code(),
                self::CITY        => $this->get_city(),
                self::PHONE       => $this->get_phone(),
                self::EMAIL       => $this->get_email(),
            ];
    }

}
