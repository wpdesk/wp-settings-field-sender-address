<?php
/**
 * Assets.
 *
 * @package WpDesk\WooCommerce\ShippingMethod\Fields\SenderAddress
 */

namespace WpDesk\WooCommerce\ShippingMethod\Fields\SenderAddress;

class Assets {

    public function enqueue( $base_url, $suffix = '', $scripts_version = '' ) {
        wp_register_style( 'fs_sender_address_css', trailingslashit( $base_url ) . 'assets/css/style.css', array(), $scripts_version );
        wp_enqueue_style( 'fs_sender_address_css' );

        wp_enqueue_script( 'fs_sender_address', trailingslashit( $base_url ) . 'assets/js/settings-sender-address.js', array(), $scripts_version );
    }

}
