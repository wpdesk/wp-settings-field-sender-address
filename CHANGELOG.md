## [1.1.0] - 2023-06-26
### Removed
- Packer composer library

## [1.0.2] - 2020-02-24
### Added
- non unique identifier info 

## [1.0.1] - 2020-02-20
### Fixed
- slashes in inputs

## [1.0.0] - 2020-02-18
### Added
- init
