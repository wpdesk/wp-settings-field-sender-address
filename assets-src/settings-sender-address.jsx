import React from 'react';
import ReactDOM from 'react-dom';
import SenderAddress from './components/sender-address.jsx';

document.addEventListener('DOMContentLoaded', function () {
    let elements = document.getElementsByClassName("settings-field-sender-address");
    let i;
    for (i = 0; i < elements.length; i++) {
        let element = elements[i];
        ReactDOM.render(<SenderAddress addresses={element.dataset.value} name={element.dataset.name} labels={element.dataset.labels}/>, document.getElementById(element.id));
    }
});
