import React, { Component } from 'react';
import AddressInput from "./address-input.jsx";

/**
 * Can display address fields in table rows.
 */
export default class AddressRow extends React.Component {

    /**
     * @param props
     */
    constructor (props) {
        super(props);

        /**
         * @type {
         *  {
         *      address: { id: number, unique: boolean, address_id: string, company: string, name: string, address: string, postal_code: string, city: string, phone: string, email: string },
         *      index: number,
         *      inputNamePrefix: string,
         *      labels: {}
         *  }
         * }
         */
        this.state = {
            address: props.address,
            index: props.index,
            inputNamePrefix: props.name,
            labels: props.labels,
        }
    }

    /**
     * @returns {*}
     */
    render () {
        let field_name = this.state.inputNamePrefix + '[' + this.state.index + ']';
        let address = this.state.address;
        let labels = this.state.labels;

        return (
            <tr id={this.state.inputNamePrefix + '_' + this.state.index} className='sender-address'>
                <td className={address.unique ? '' : 'non-unique'}>
                    <AddressInput className={address.unique ? '' : 'non-unique'} address={address} field='address_id' name={field_name + '[address_id]'} value={this.state.address.address_id} />
                    <span>{labels.non_unique_id}</span>
                </td>
                <td>
                    <AddressInput address={address} field='company' name={field_name + '[company]'} value={this.state.address.company} />
                </td>
                <td>
                    <AddressInput address={address} field='name' name={field_name + '[name]'} value={this.state.address.name} />
                </td>
                <td>
                    <AddressInput address={address} field='address' name={field_name + '[address]'} value={this.state.address.address} />
                </td>
                <td>
                    <AddressInput address={address} field='postal_code' name={field_name + '[postal_code]'} value={this.state.address.postal_code} />
                </td>
                <td>
                    <AddressInput address={address} field='city' name={field_name + '[city]'} value={this.state.address.city} />
                </td>
                <td>
                    <AddressInput address={address} field='phone' name={field_name + '[phone]'} value={this.state.address.phone} />
                </td>
                <td>
                    <AddressInput address={address} field='email' type='email' name={field_name + '[email]'} value={this.state.address.email} />
                </td>
                <td>
                    <button data-index={this.state.index} className="button sender-address-delete" onClick={this.props.handleClickDelete}>{labels.button_delete}</button>
                </td>
            </tr>
        )
    }
}
