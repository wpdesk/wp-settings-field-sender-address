import React, { Component } from 'react';

/**
 * Can display address field.
 */
export default class AddressInput extends React.Component {

    /**
     * @param props
     */
    constructor (props) {
        super(props);

        /**
         * @type {string}
         */
        let type = 'text';
        if ( props.type ) {
            type = props.type;
        }

        /**
         * @type {
         *  {
         *      address: { id: number, address_id: string, company: string, name: string, address: string, postal_code: string, city: string, phone: string, email: string },
         *      field: string,
         *      name: string,
         *      type: string,
         *      value: string
         *  }
         * }
         */
        this.state = {
            address: props.address,
            field: props.field,
            name: props.name,
            type: type,
            value: props.value,
        };

        this.handleChange = this.handleChange.bind(this);
    }

    /**
     * @param props
     */
    componentWillReceiveProps(props){
        this.setState({
            address: props.address,
            field: props.field,
            name: props.name,
            value: props.value
        });
    }

    /**
     * @param {React.ChangeEvent<HTMLInputElement>} event
     */
    handleChange(event) {
        let state = this.state;
        state.value = event.target.value;
        state.address[state.field] = event.target.value;
        this.setState(state);
    }

    /**
     * @returns {*}
     */
    render () {
        let className = 'input-text regular-input ' + this.props.className;

        return (
            <input
                className={className}
                type={this.state.type}
                name={this.state.name}
                value={this.state.value}
                required="required"
                onChange={this.handleChange}
            />
        )
    }
}
