import React, { Component } from 'react';
import AddressRow from "./address-row.jsx";

/**
 * Can display sender addresses in HTML table.
 * User can add (button Add) and delete (button Delete) sender address.
 */
export default class SenderAddressTable extends React.Component {

    /**
     * @param props
     */
    constructor (props) {
        super(props);
        let elements = 0;
        let addresses = props.addresses;
        let addresses_ids = {};
        addresses.forEach(function(element){
            element.id = elements;
            elements++;
            // Mark non unique address_ids.
            element.unique = addresses_ids[element.address_id] === undefined;
            addresses_ids[element.address_id] = element.address_id;
        });
        /**
         * @type {{addresses: array, elements: number, name: *, labels: ([]|NodeListOf<HTMLLabelElement>|any|*[]|NodeListOf<HTMLLabelElement>)}}
         */
        this.state = {
            addresses: addresses,
            name: props.name,
            elements: elements,
            labels: props.labels,
        };
    }

    /**
     * @param {MouseEvent} event
     */
    handleClickAdd(event) {
        event.preventDefault();
        let state = this.state;
        state.elements++;
        state.addresses.push({
            id: state.elements,
            unique: true,
            address_id: '',
            company:  '',
            name:  '',
            address:  '',
            postal_code:  '',
            city:  '',
            phone:  '',
            email:  '',
        });
        this.setState( state );
    }

    /**
     * @param {MouseEvent} event
     */
    handleClickDelete(event) {
        event.preventDefault();
        this.state.addresses = this.state.addresses.filter(address => address.id != event.target.dataset.index);
        this.setState(this.state);
    }

    /**
     * @returns {*}
     */
    render () {
        let labels = this.state.labels;

        return (
                <table className="sender-address">
                    <thead>
                    <tr>
                        <td className="cell-string">{labels.address_id}</td>
                        <td className="cell-string">{labels.company}</td>
                        <td className="cell-string">{labels.name}</td>
                        <td className="cell-string">{labels.address}</td>
                        <td className="cell-string">{labels.postal_code}</td>
                        <td className="cell-string">{labels.city}</td>
                        <td className="cell-string">{labels.phone}</td>
                        <td className="cell-string">{labels.email}</td>
                        <td className="cell-action"> </td>
                    </tr>
                    </thead>
                    <tbody>
                    {
                        this.state.addresses.map((address) =>
                            <AddressRow
                                key={address.id}
                                index={address.id}
                                address={address}
                                name={this.props.name}
                                handleClickDelete={this.handleClickDelete.bind(this)}
                                labels={labels}
                            />
                        )
                    }
                    </tbody>
                    <tfoot>
                        <tr>
                            <td colSpan="8">
                                <button className="button sender-address-add" onClick={this.handleClickAdd.bind(this)}>{labels.button_add}</button>
                            </td>
                        </tr>
                    </tfoot>
                </table>
        )
    }
}
